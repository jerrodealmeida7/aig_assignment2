### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 2c18e6ee-faa2-4a25-ab1b-cb2b714419c3
begin
	using Pkg;
	Pkg.build("PyCall")
	Pkg.add("PyCall")
	Pkg.add("PlutoUI")
	Pkg.add("TensorFlow")
	Pkg.add("Plots")
	Pkg.add("Distributions")
	Pkg.add("DataFrames")
	Pkg.add("Flux")
	Pkg.add("PyPlot")
end

# ╔═╡ a979ffd7-6672-4f18-ab2a-ca5fd131d552
begin
	using TensorFlow
	using PlutoUI
	using Flux, Flux.Data.MNIST, Statistics
	using Flux: onehotbatch, onecold, crossentropy, throttle
	using Printf, BSON
	using Distributions
	using DataFrames
	using PyPlot
end

# ╔═╡ a2fc434a-4713-42e8-a55b-e0c2f8f2afd4
train_path = "/home/miracle/Desktop/chest_xray/train"

# ╔═╡ 89c49f94-d3e6-4c64-bf9d-5e805f7a781a
test_path = "/home/miracle/Desktop/chest_xray/test"

# ╔═╡ 9525d3e7-0f08-4593-ab24-eddfafc5b4d1
valid_path = "/home/miracle/Desktop/chest_xray/val"

# ╔═╡ 489f9ebb-bc7e-459a-9d72-e2366c3356a4
begin
	length(valid_path)
	length(test_path)
	length(train_path)
end

# ╔═╡ 53eb64a2-442a-4f5c-94ca-341ec4d42f87
function make_minibatch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:9)
    return (X_batch, Y_batch)
end

# ╔═╡ 55246246-0b25-45b3-8177-877e3f500a93
begin
	function readimages(nimages)
	    img=load("/home/miracle/Desktop/chest_xray/val")
	    nx,ny=size(img)
	    @show nx,ny,nimages
	    images=zeros(RGB{Normed{UInt8,8}},nx,ny,nimages)
	    for i=1:nimages
	        img=load("/home/miracle/Desktop/chest_xray/val")
	        images[:,:,i].=img[:,:]
	    end
	    images
	end
	show
end

# ╔═╡ 643fa4f4-ab77-4e55-b922-73ce17d8d87f
begin
	data = Iterators.repeated((train_path, test_path), 100)
	
	model = Chain(
	  Dense(64, 64, train_path),
	  Dense(64, 32, train_path),
	  Dense(32, 1))
	
	loss(x, y) = Flux.mse(model(x), y)
	ps = Flux.params(model)
	opt = ADAM() 
	evalcb = () -> @show(loss(train_path, test_path))

end

# ╔═╡ 2aed15e1-fce2-438d-bf8e-1c8f46fd5dc1
function load_normal(norm_path)
    norm_files = np.array(os.listdir(norm_path))
    norm_labels = np.array(["normal"]*len(norm_files))
    
    norm_images = []
    for image in tqdm(norm_files)
        image = cv2.imread(norm_path + image)
        image = cv2.resize(image, dsize=(200,200))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        norm_images.append(image)
        
    norm_images = np.array(norm_images)
    return norm_images, norm_labels

function load_pneumonia(pneu_path)
    pneu_files = np.array(os.listdir(pneu_path))
    pneu_labels = np.array([pneu_file.split('_')[1] for pneu_file in pneu_files])
    
    pneu_images = []
    for image in tqdm(pneu_files)
        image = cv2.imread(pneu_path + image)
        image = cv2.resize(image, dsize=(200,200))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        pneu_images.append(image)
        
    pneu_images = np.array(pneu_images)
			
    return pneu_images, pneu_labels
end

# ╔═╡ 6c6802a8-0582-4db6-9ea8-acd31b5efaf3
function norm_images, norm_labels = load_normal("kaggle datasets download -d paultimothymooney/chest-xray-pneumonia/NORMAL")pneu_images, pneu_labels = load_pneumonia("kaggle datasets download -d paultimothymooney/chest-xray-pneumonia/chest_xray/train/PNEUMONIA/")
end

# ╔═╡ 6c048f97-86d8-4e7d-825e-3fc3183493f4
begin
	X_train = np.append(norm_images, pneu_images, axis=0)
	y_train = np.append(norm_labels, pneu_labels)
end

# ╔═╡ 6b2d87ba-f3ce-4e27-95a7-febc65b331fe
begin
	function norm_images_test, norm_labels_test = load_normal("/kaggle/input/chest-xray-pneumonia/chest_xray/test/NORMAL/")pneu_images_test, pneu_labels_test=load_pneumonia("/kaggle/input/chest-xray-pneumonia/chest_xray/test/PNEUMONIA/")X_test =np.append(norm_images_test, pneu_images_test, axis=0)
	y_test = np.append(norm_labels_test, pneu_labels_test)
	end
end

# ╔═╡ dd048144-f9dd-4ddc-b702-7e83058ec410
begin
	input1 = Input(shape=(X_batch.shape[1], X_batch.shape[2], 1))
	
	cnn = Conv2D(16, (3, 3), activation="relu", strides=(1, 1), 
	    padding="same")(input1)
	cnn = Conv2D(32, (3, 3), activation="relu", strides=(1, 1), 
	    padding="same")(cnn)
	cnn = MaxPool2D((2, 2))(cnn)
	
	cnn = Conv2D(16, (2, 2), activation="relu", strides=(1, 1), 
	    padding="same")(cnn)
	cnn = Conv2D(32, (2, 2), activation="relu", strides=(1, 1), 
	    padding="same")(cnn)
	cnn = MaxPool2D((2, 2))(cnn)
	
	cnn = Flatten()(cnn)
	cnn = Dense(100, activation="relu")(cnn)
	cnn = Dense(50, activation="relu")(cnn)
	output1 = Dense(3, activation="softmax")(cnn)
	
	
end

# ╔═╡ 3bb3bb2c-ad2d-468b-902b-1eb4f1cacc4b
model.compile(loss="categorical_crossentropy", 
              optimizer="adam", metrics=["acc"])

# ╔═╡ d6b62f1f-b29e-4a76-8b98-842e52cc45f8
begin
	plot.figure(figsize=(8,6))
	plot.title("Accuracy scores")
	plot.plot(history.history["acc"])
	plot.plot(history.history["val_acc"])
	plot.legend(["acc", "val_acc"])
	plot.show()plt.figure(figsize=(8,6))
	plot.title("Loss value")
	plot.plot(history.history["loss"])
	plot.plot(history.history["val_loss"])
	plot.legend(["loss", "val_loss"])
	plot.show()
end

# ╔═╡ ba7c1c68-eba2-491b-9700-0d3ba766ce0c
begin
	predictions = model.predict(X_batch)
	predictions = one_hot_encoder.inverse_transform(predictions)
end

# ╔═╡ 16546ed1-79e4-4c34-ac2b-9506a9a21c7b
begin
	classnames = ["bacteria", "normal", "virus"]plot.figure(figsize=(8,8))
	plot.title("Confusion matrix")
	sns.heatmap(cm, cbar=False, xticklabels=classnames, yticklabels=classnames, fmt='d', annot=True, cmap=plt.cm.Blues)
	plot.xlabel("Predicted")
	plot.ylabel("Actual")
	plot.show()
end

# ╔═╡ Cell order:
# ╠═2c18e6ee-faa2-4a25-ab1b-cb2b714419c3
# ╠═a979ffd7-6672-4f18-ab2a-ca5fd131d552
# ╠═a2fc434a-4713-42e8-a55b-e0c2f8f2afd4
# ╠═89c49f94-d3e6-4c64-bf9d-5e805f7a781a
# ╠═9525d3e7-0f08-4593-ab24-eddfafc5b4d1
# ╠═489f9ebb-bc7e-459a-9d72-e2366c3356a4
# ╠═53eb64a2-442a-4f5c-94ca-341ec4d42f87
# ╠═55246246-0b25-45b3-8177-877e3f500a93
# ╠═643fa4f4-ab77-4e55-b922-73ce17d8d87f
# ╠═2aed15e1-fce2-438d-bf8e-1c8f46fd5dc1
# ╠═6c6802a8-0582-4db6-9ea8-acd31b5efaf3
# ╠═6c048f97-86d8-4e7d-825e-3fc3183493f4
# ╠═6b2d87ba-f3ce-4e27-95a7-febc65b331fe
# ╠═dd048144-f9dd-4ddc-b702-7e83058ec410
# ╠═3bb3bb2c-ad2d-468b-902b-1eb4f1cacc4b
# ╠═d6b62f1f-b29e-4a76-8b98-842e52cc45f8
# ╠═ba7c1c68-eba2-491b-9700-0d3ba766ce0c
# ╠═16546ed1-79e4-4c34-ac2b-9506a9a21c7b
